require('dotenv').config();
const cors = require('cors')
const morgan = require('morgan')
const express = require('express');
const bodyParser = require('body-parser');
const sgMail = require('@sendgrid/mail');

const app = express()

app.use(cors())
app.use(morgan('tiny'))
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(express.json())

//ENDPOINT NOT USED ANYMORE
// app.post('/email', (req, res) => {
//     console.log('Got body:', req.body);
//     res.sendStatus(200);
//     sgMail.setApiKey(process.env.VUE_APP_SENDGRID_KEY);
//     const msg = {
//         to: 'techteamcto@gmail.com',
//         from: 'noreply@odk.ai',
//         subject: 'Mail from ODK-website',
//         text: req.body.formMessage,
//         replyTo: req.body.formEmail
//     };
//     sgMail.send(msg);
// });

const port = process.env.PORT || 3000
app.listen(port, () => console.log(`Listening on port ${port}....`))