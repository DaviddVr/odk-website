import Vue from 'vue'
import VueRouter from 'vue-router'
import HomePage from '@/components/HomePage.vue'
import SetupPage from '@/components/SetupPage.vue'
import ParticipationPage from '@/components/ParticipationPage.vue'
import RoadmapPage from '@/components/RoadmapPage.vue'
import FaqPage from '@/components/FaqPage.vue'
import AboutPage from '@/components/AboutPage.vue'

Vue.use(VueRouter)

let router = new VueRouter({
  // mode : "history",

  routes: [{
      path: '/',
      name: 'home-page',
      component: HomePage,
    },
    {
      path: '/setup',
      name: 'setup-page',
      component: SetupPage,
    },
    {
      path: '/participation',
      name: 'paticipation-page',
      component: ParticipationPage,
    },
    {
      path: '/about',
      name: 'about-page',
      component: AboutPage,
    },
    {
      path: '/roadmap',
      name: 'roadmap-page',
      component: RoadmapPage,
    },
    {
      path: '/faq',
      name: 'faq-page',
      component: FaqPage,
    },
    //   {
    //     path: '*',
    //     name: 'error-page',
    //     component: ErrorPage,
    //     // props: true, // allows us to pass params to Dashboard component
    // },
  ],
  scrollBehavior(to, from, savedPosition) {
    return {
      x: 0,
      y: 0
    };
  }
})

/*
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth) && localStorage.getItem('username') == null)
  {
    next({ path: '/', query: { redirect: to.fullPath }});
  } 
  else {
    next();
  }
});
*/

export default router