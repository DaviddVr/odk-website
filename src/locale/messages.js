/* These contain the translations used by all components */
/* Components can also use their own translations */

const messages = {
    en: {
        test: 'A text!',
        setup: "Setup",
        participation: "Participate",
        roadmap: "Roadmap",
        faq: "FAQ",
        contact: "Contact Us",
        policy: "Privacy policy",
        about: "About",
    },
    nl: {
        test: 'Een tekst',
        setup: "Installatie",
        participation: "Doe mee",
        roadmap: "Roadmap",
        faq: "FAQ",
        contact: "Neem contact op",
        policy: "Privacybeleid",
        about: "Over ons",
    }
}



export default messages