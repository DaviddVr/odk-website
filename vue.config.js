const path = require('path');

module.exports = {
  //...
  configureWebpack: {
    resolve: {
      alias: {
        "animation.gsap": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js'),
      }
    }
  }
};